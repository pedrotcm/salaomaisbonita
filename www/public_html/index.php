    <!DOCTYPE html>
    <html lang="en">
    <head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<title>MAIS BONITA</title>
    	<!-- Google Font -->
    	<link href='css/google_font_01.css' rel='stylesheet' type='text/css'>

    	<link href='css/google_font_02.css' rel='stylesheet' type='text/css'>
    	<!-- Bootstrap -->
    	<link href="css/bootstrap.min.css" rel="stylesheet">

    	<!-- Owl Carousel Assets -->
    	<link href="css/owl.carousel.css" rel="stylesheet">
    	<link href="css/owl.theme.css" rel="stylesheet">


    	<!-- Font Awesome -->
    	<link href="css/font-awesome.min.css" rel="stylesheet">

    	<!-- Instagram -->
    	<link rel="stylesheet" href="css/instagram.css"/>

    	<!-- PrettyPhoto -->
    	<link href="css/prettyPhoto.css" rel="stylesheet">

    	<!-- Favicon -->
    	<link rel="shortcut icon" type="image/png" href="images/favicon.png" />

    	<!-- Style -->
    	<link href="css/style.css" rel="stylesheet">

    	<link href="css/animate.css" rel="stylesheet">
    	<!-- Responsive CSS -->
    	<link href="css/responsive.css" rel="stylesheet">
    	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
    	  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    	  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    	  <![endif]-->

    	<!--
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-78909803-1', 'auto');
      ga('send', 'pageview');

    </script>
-->
    <!--
    <!-- Facebook Pixel Code 
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '245786829216560'); // Insert your pixel ID here.
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=245786829216560&ev=PageView&noscript=1"/></noscript>
-->

<script src="js/instafeed.min.js"></script>
<script type="text/javascript">
	var userFeed = new Instafeed ({
		get: 'user',
		userId: '247005067',
		accessToken: '247005067.1677ed0.dbd60e32c59f43798b45f7cb07cd3675',
		template: '<div class="instafeed-size"><a href="{{link}}" target="_blank"><img src="{{image}}" /></a></div>' ,
		resolution: 'low_resolution' ,
		limit: '5'
	});
	userFeed.run();
</script>

</head>

<body>

	<!-- FB SDK -->
    <!--
    <div id="fb-root"></div>
    <script>
    	(function(d, s, id) {
    	  var js, fjs = d.getElementsByTagName(s)[0];
    	  if (d.getElementById(id)) return;
    	  js = d.createElement(s); js.id = id;
    	  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
    	  fjs.parentNode.insertBefore(js, fjs);
    	}(document, 'script', 'facebook-jssdk'));
    </script>
-->
<!-- PRELOADER -->
<div class="spn_hol">
	<div class="spinner">
		<div class="bounce1"></div>
		<div class="bounce2"></div>
		<div class="bounce3"></div>
	</div>
</div>

<!-- END PRELOADER -->

<div class="section_overlay">
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container">

			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Menu</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#HOME">
					<img class="logoImage">
				</a>
				<div class="show-only-mobile" style="text-align:center;">

					<a class="social_icons-header" target="_blank" href="https://www.facebook.com/salaomaisbonita/"><i class="fa fa-facebook-official" style="font-size:25px;color:#4a90e2;"></i></a>
					<a class="social_icons-header" target="_blank" href="http://instagram.com/_u/salaomaisbonita/"><i class="fa fa-instagram" style="font-size:25px;color:#fb3958;"></i></a>
				</div>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<!-- NAV -->
					<li><a id="homeMenu" href="#HOME"></a> </li>
					<li><a href="#SCHEDULING">AGENDAMENTO</a> </li>
					<li><a href="#ABOUT">SOBRE NÓS</a> </li>
					<li><a href="#EMPLOYEES">PROFISSIONAIS</a> </li>
					<li><a href="#SERVICES">SERVIÇOS</a> </li>
					<li><a href="#CONTACT">LOCALIZAÇÃO/CONTATO</a> </li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container- -->
	</nav>
</div>

     <!-- =========================
         START ABOUT US SECTION
         ============================== -->
         <section class="header parallax home-parallax page" id="HOME">

         	<div class="inner_home container page" style="width: 100%;padding: 80px 0px 0px 0px">
         		<script id="headerDynamicScript" src="content_header.js?v=<?php echo mt_rand(); ?>"></script>
         	</div>

         </section>

         <!-- END HEADER SECTION -->

    <!-- =========================
         START SCHEDULING NOW 
         ============================== -->
         <section class="download page" id="SCHEDULING">
         	<div class="container">
         		<div class="row">
         			<div class="col-md-10 col-md-offset-1">
         				<div class="section_title">
         					<h2>AGENDAMENTO</h2>
         					<form method="GET" action="http://www.salaomaisbonita.com/sistema/pages/clientScheduling/access.faces">
         						<br/>
         						<span>*Informe o telefone no formato (99) 99999-9999</span>
         						<div class="input-group input-group-lg home_text">
         							<span class="input-group-addon" id="phone">Telefone</span>
         							<input name="phone" id="phoneInput" type="tel" class="form-control" maxlength="15" 
         							pattern="\([0-9]{2}\)[\s][0-9]{5}-[0-9]{4}" placeholder="(00) 00000-0000" 
         							aria-describedby="basic-addon1" required="true">
         						</div>
         					</br>
         					<span>*Informe a data de aniversário no formato 99/99 (dia/mês)</span>
         					<div name="birthDate" class="input-group input-group-lg home_text">
         						<span class="input-group-addon" id="birthDate">Aniversário</span>
         						<input name="birthDate" id="birthDateInput" type="tel" class="form-control" placeholder="00/00" 
         						aria-describedby="basic-addon1" required="true" pattern="\d{2}/\d{2}">
         					</div>
         					<div class="col-md-12" style="text-align:center;">
         						<p><button type="submit" class="btn btn-default">Ver/Continuar Agendamento</button></p>
         					</br><a target="_blank" href="http://www.salaomaisbonita.com/sistema/admin">Acesso Restrito</a>
         					<!--</br>Realize seu agendamento. Em breve entraremos em contato com você!-->
         				</div>
         			</form>

         		</div>
         	</div>
         </div>
     </div>

 </section>
 <!-- END SCHEDULING -->



     <!-- =========================
         START ABOUT US SECTION
         ============================== -->


         <section class="about page" id="ABOUT">
         	<div class="container">
         		<div class="row">
         			<div class="col-md-10 col-md-offset-1">
         				<div class="section_title">
         					<h2>SOBRE NÓS!</h2>
         					<p>
         						O "Mais Bonita" nasceu com a finalidade de proporcionar aos nossos clientes um espaço aconchegante, caloroso e humanizado, oferecendo serviços de extrema qualidade na área de beleza capilar e estética.</p>

         						<p>
         							Hoje contamos com uma equipe de excelência, altamente capacitada e sempre atualizada. Nossos profissionais frequentemente estão realizando cursos e treinamentos nas melhores academias nacionais e internacionais, acompanhando todas as tendências e inovações do que há de melhor no mercado da beleza. 
                    </p>
                    <p>
                      Nossa missão será sempre superar as expectativas dos nossos clientes.
         						</p>

         						<p>
                      NOSSOS VALORES!</br></br>
         							Amor à profissão</br> 
         							Atendimento diferenciado</br> 
         							Qualidade</br> 
         							Inovação</br> 
         							Comprometimento</br> 
         							Bem-estar</br> 
         							Valorização humana</br> 
         						</p>

         					</div>
         				</div>

         			</div>
         		</div>
         	</section>
         	<!-- End About Us -->

     <!-- =========================
         START EMPLOYEES
         ============================== -->

         <div id="EMPLOYEES" class="section_title">
         	<h2>PROFISSIONAIS</h2>
         </div>
         <section class="testimonial parallax">
         	<div class="section_overlay">
         		<div class="container">
         			<div class="row">
         				<div class="col-md-12">
         					<div class="item active">
         						<div class="container">
         							<div class="row">
         								<div class="col-md-4 text-center">
         									<!-- IMAGE -->
         									<img src="images/professionals/teresa_cristina.jpg" alt="">
         									<div class="testimonial_caption">
         										<!-- DESCRIPTION -->  
         										<h2>Teresa Cristina</h2>
         										<h4><span>"Corte, Penteado, Química, Tratamento e Visagismo"</span></h4>
         										<!--<p>“Especialista em finalização e corte masculino e feminino”</p>-->
         									</div>
         								</div>
         								<div class="col-md-4 text-center">
         									<!-- IMAGE -->
                                            <img src="images/professionals/ivan_lee.jpg" alt="">
                                            <div class="testimonial_caption">
                                               <!-- DESCRIPTION -->  
                                               <h2>Ivan Lee</h2>
                                               <h4><span>"Corte, Escova, Makeup, Penteado, Química, Tratamento e Visagismo"</span></h4>
                                               <!--<p>“Especialista em finalização e corte masculino e feminino”</p>-->
                                           </div>
                                       </div>
                                       <div class="col-md-4 text-center">
                                          <!-- IMAGE -->
                                          <img src="images/professionals/marcia-regina.jpg" alt="">
                                          <div class="testimonial_caption">
                                           <!-- DESCRIPTION -->  
                                           <h2>Márcia Regina</h2>
                                           <h4><span>"Drenagem Linfática, Limpeza de Pele, Massagem Redutora e Massagem Relaxante"</span></h4>
                                           <!--<p>“Especialista em finalização e corte masculino e feminino”</p>-->
                                       </div>
                                   </div>
                               </div>
                               <div class="row">
                                 <div class="col-md-4 text-center">
                                  <!-- IMAGE -->
                                  <img src="images/professionals/fran.jpg" alt="">
                                  <div class="testimonial_caption">
                                   <!-- DESCRIPTION -->  
                                   <h2>Fran</h2>
                                   <h4><span>"Depilação, Escova, Manicura, Pedicura, Química e Tratamento"</span></h4>
                                   <!--<p>“Especialista em finalização e corte masculino e feminino”</p>-->
                               </div>
                           </div>
                           <div class="col-md-4 text-center">
                              <!-- IMAGE -->
                              <img src="images/professionals/thais.jpg" alt="">
                              <div class="testimonial_caption">
                               <!-- DESCRIPTION -->  
                               <h2>Thais</h2>
                               <h4><span>"Depilação, Escova, Makeup, Penteado, Química e Tratamento"</span></h4>
                               <!--<p>“Especialista em finalização e corte masculino e feminino”</p>-->
                           </div>
                       </div>
                       <div class="col-md-4 text-center">
                        <!-- IMAGE -->
                        <img src="images/professionals/luciana.jpg" alt="">
                        <div class="testimonial_caption">
                            <!-- DESCRIPTION -->  
                            <h2>Luciana</h2>
                            <h4><span>"Depilação, Design de Sobrancelhas, Manicura e Pedicura"</span></h4>
                            <!--<p>“Especialista em finalização e corte masculino e feminino”</p>-->
                        </div>
                    </div>
                   </div>
                   <div class="row">
                    <div class="col-md-4 text-center">
                        <!-- IMAGE -->
                        <img src="images/professionals/silvia-cristina.jpg" alt="">
                        <div class="testimonial_caption">
                            <!-- DESCRIPTION -->  
                            <h2>Sílvia Cristina</h2>
                            <h4><span>"Makeup"</span></h4>
                            <!--<p>“Especialista em finalização e corte masculino e feminino”</p>-->
                        </div>
                    </div>
               <div class="col-md-4 text-center">
                  <!-- IMAGE -->
                  <img src="images/professionals/danielle.jpg" alt="">
                  <div class="testimonial_caption">
                   <!-- DESCRIPTION -->  
                   
                   <h2>Danielle</h2>
                		<h4><span>"Depilação, Design de Sombrancelhas, Escova, Manicura, Pedicura, Química e Tratamento"</span></h4>
                   <!--<p>“Especialista em finalização e corte masculino e feminino”</p>-->
               		</div>
           		</div>
                <div class="col-md-4 text-center">
                      <!-- IMAGE -->
                      <img src="images/professionals/marcia-cristina.jpg" alt="">
                      <div class="testimonial_caption">
                       <!-- DESCRIPTION -->  
                       <h2>Márcia Cristina</h2>
                       <h4><span>"Escova, Química e Tratamento"</span></h4>
                       <!--<p>“Especialista em finalização e corte masculino e feminino”</p>-->
                   </div>
               </div>
               
       </div>
       <div class="row">
           <div class="col-md-4 col-md-offset-2 text-center">
            <!-- IMAGE -->
            <img src="images/professionals/ana-cristina.jpg" alt="">
            <div class="testimonial_caption">
                <!-- DESCRIPTION -->  
                <h2>Ana Cristina</h2>
              	 <h4><span>"Design de Sombrancelhas"</span></h4>
                <!--<p>“Especialista em finalização e corte masculino e feminino”</p>-->
            </div>
        </div>
        <div class="col-md-4 text-center">
           <!-- IMAGE -->
           <img src="images/professionals/michele.jpg" alt="">
           <div class="testimonial_caption">
            <!-- DESCRIPTION -->  
            <h2>Michele</h2>
            <h4><span>"Manicura e Pedicura"</span></h4>
            <!--<p>“Especialista em finalização e corte masculino e feminino”</p>-->
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<!-- END EMPLOYEES -->


    <!-- ==================
    	START SERVICES
    	================== -->

    	<section class="about page" id="SERVICES">
    		<div class="container">
    			<div class="row">
    				<div class="col-md-10 col-md-offset-1">
    					<div class="section_title">
    						<h2>SERVIÇOS</h2>
    					</div>
    				</div>
    			</div>
    		</div>
    		<div class="container">
    			<div class="row">
    				<div class="col-md-4">
    					<div class="about_phone wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s">
    						<div>
    							<h3>CABELO</h3>
    						</div>
    					</div>
    					<div class="about_phone wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s">

    						<p>
    							<ul>
    								Alisamento Liso Molecular</br>
                                    Cauterização</br>
                                    Coloração</br>
                                    Corte</br>
                                    Dedinho</br>
                                    Escova</br>
                                    Hidratação</br>
                                    Higienização</br>
                                    Luzes / Iluminação</br>
                                    Matização</br>
                                    Penteados</br>
                                    Reconstrução</br>
                                </ul>
                            </p>
                        </div>
                       

            <div class="about_phone wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s">
                    <h3>ESTÉTICA</h3>
                </div>
                <div class="about_phone wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s">
                    <p>
                        <ul>
                            Drenagem Linfática</br>
                            Hidratação da Pele</br>
                            Limpeza de Pele Profunda</br>
                            Massagem Redutora/Modeladora</br>
                            Massagem Relaxante</br>
                        </ul>
                    </p>
                </div>  

                </div>
                <div class="col-md-4">

                    <div class="about_phone wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s">

                      <h3>DEPILAÇÃO</h3>
                  </div>
                  <div class="about_phone wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s">
                      <p>
                         <ul>
                            Depilação com Cera</br>
                            Depilação com Linha</br>
                            Axilas</br>
                            Buço</br>
                            Contorno/Completo</br>
                            Coxa</br>
                            Perna</br>
                            Rosto</br>
                        </ul>
                    </p>
                </div>

            <div class="about_phone wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s">

                    <h3>UNHA</h3>
                </div>
                <div class="about_phone wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s">
                   <p>
                     <ul>
                        Esmaltação</br>
                        Esmaltação em Gel</br>
                        Francesinha</br>
                        Manicura</br>
                        Pedicura</br>
                        Pintura</br>
                        Reflexologia dos Pés</br>
                        SPA Pé e Mão</br>
                        Unhas Decoradas</br>
                    </ul>
                </p>
            </div>

            </div>
            <div class="col-md-4">

             <div class="about_phone wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s">

                          <h3>SOBRANCELHA</h3>
                          </div>
                          <div class="about_phone wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s">
                          <p>

                             <ul>
                                Cera</br>
                                Coloração</br>
                                Design</br>
                                Linha</br>
                                Henna</br>
                                Pinça</br>
                            </ul>
                        </p>
                    </div>

            <div class="about_phone wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s">

                    <h3>MAQUIAGEM</h3>
                </div>

                <div class="about_phone wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s">
                    <p>
                        <ul>
                            Makeup Artística</br>
                            Makeup Casual</br>
                            Makeup Comemorações</br>
                            Aplicação de Cílios </br>
                        </ul>
                    </p>
                </div>

            <div class="about_phone wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s">

              <h3>PACOTE</h3>
              <p>
              </div>
              <div class="about_phone wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s">
                 <ul>
                    Pacote Formatura</br>
                    Pacote Noiva</br>
                    Pacote Personalizado</br>
                </ul>
            </p>
        </div>

    </div>
</div>
</div>

</section>

<!-- END SERVICES -->    

    <!-- =========================
         START INSTAGRAM
         ============================== -->    

         <section class="instagram-area section-padding page">
         	<div class="instagram-area-bg"></div>
         	<div class="container wow fadeIn">
         		<div class="row">
         			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
         				<div class="area-title-insta text-center">
         					<h4><i class="fa fa-instagram"></i> siga-nos • <a href="http://instagram.com/_u/salaomaisbonita/"" target="_blank">@salaomaisbonita</a></h4>
         				</div>
         			</div>
         		</div>
         		<div class="row">
         			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
         				<div id="instafeed" class="text-center"></div>
         			</div>
         		</div>
         	</div>
         </section>
         <!-- END INSTAGRAM -->


    <!-- =========================
         START CONTCT FORM AREA
         ============================== -->
         <section class="contact page" id="CONTACT">
         	<div class="section_overlay">
         		<!-- Start Contact Section Title-->
         		<div class="section_title" >
         			<h2>LOCALIZAÇÃO / CONTATO</h2>
         		</div>
         		<div class="google-maps">
         			<p style="text-align:center;">
         				<iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCVPKwrK-FxWaTUuJdJyJ3cSiwxfZ_SIiQ&q=Salao+Mais+Bonita,Teresina" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
         			</p> 
         		</div>

         		<div class="container">
         			<div class="col-md-10 col-md-offset-1 wow bounceIn">

         				<div style="text-align:center;padding-top:5px !important">
         					<p>
         						<a class="btn-waze" target="_blank" href="https://waze.com/ul/h7p1e4rwtr">
         							<span>Ver no <b>Waze</b></span> 
         						</a>
         					</p>
         					<p>
         						<a style="text-decoration:none;background-color:#4285f4;border-radius:2px;color:#fff;display:inline-block;font-weight:bold;min-height:40px;min-width:300px;text-align:center" href="https://goo.gl/app/maps?link=https://goo.gl/maps/PXrN4Xcr5Zx" target="_blank"> 
         							<span style="line-height:40px">Ver no Google MAPS</span> 
         						</a>
         					</p>				

         					<div class="section_title" style="padding-top:5px !important">
         						<p style="margin-top:5px !important">Av. Homero Castelo Branco, 3072 - Ininga, Teresina - PI</br>
         							<a href="https://api.whatsapp.com/send?phone=5586999722657&text=Ol%C3%A1%20Salão Mais Bonita" target="_blank" style="color:green"><i class="fa fa-whatsapp"></i> (86) 99972-2657</a>
         					</br>(86) 3231-1426</br>
         				</br>
         			</p>
         			<div class="social_icons">
         				<ul>
         					<li><a target="_blank" href="https://www.facebook.com/salaomaisbonita/"><i class="fa fa-facebook-official" style="font-size:48px;color:#4a90e2;"></i></a>
         					</li>
         					<li><a target="_blank" href="http://instagram.com/_u/salaomaisbonita/"><i class="fa fa-instagram" style="font-size:48px;color:#fb3958;"></i></a>
         					</li>
         				</ul>
         			</div>

         		</div>
         	</div>
         </div>
     </div>
 </div>
 <div class="contact_form wow bounceIn">
 	<div class="container">
 		<div class="section_title" style="padding:0px !important;text-align:center;font-weight: bold">
 			<h4> Dúvidas ou comentários, envie sua mensagem para nós! </h4>
 		</div>
 		<!-- START ERROR AND SUCCESS MESSAGE -->
 		<div class="form_error text-center">
 			<div class="name_error hide error">Por favor, informe o Nome</div>
 			<div class="email_error hide error">Por favor, informe o Email</div>
 			<div class="email_val_error hide error">Por favor, informe um Email válido</div>
 			<div class="message_error hide error">Por favor, informe a Mensagem</div>
 		</div>
 		<div class="Sucess"></div>
 		<!-- END ERROR AND SUCCESS MESSAGE -->

 		<!-- FORM -->    
 		<form role="form" method="post" action="envia.php">
 			<input id="emailD" type="hidden" name="emaildestinatario" value="maisbonita3072@gmail.com"/>
 			<div class="row">
 				<div class="col-md-4">
 					<input type="text" name="nomeremetente" class="form-control" id="name" placeholder="Nome" required>
 					<input type="email" name="emailremetente" class="form-control" id="email" placeholder="Email" required>
 					<input type="text" name="assunto" class="form-control" id="subject" placeholder="Assunto" required>
 				</div>


 				<div class="col-md-8">
 					<textarea class="form-control" name="mensagem" id="message" rows="25" cols="10" placeholder="Mensagem..." required></textarea>
 					<button type="submit" name="Submit" class="btn btn-default submit-btn form_submit">Enviar!</button>
 				</div>
 			</div>
 		</form>
 		<!-- END FORM --> 
 	</div>
 </div>

</section>
<!-- END CONTACT -->


    <!-- =========================
         FOOTER 
         ============================== -->

         <section class="copyright">
         	<h2></h2>
         	<div class="container">
         		<div class="row">
         			<div class="col-md-12">
         				<div class="scroll_top">
         					<a href="#HOME"><i class="fa fa-angle-up"></i></a>
         				</div>
         			</div>
         			<div class="col-md-12">
         				<div class="copy_right_text">
         					<!-- COPYRIGHT TEXT -->
         					<p>Copyright &copy; - Todos os direitos reservados. <span></p>
         					<p>Website By </span>  <a target="blank" href="https://br.linkedin.com/in/perfilrafaelfernandes">Doutor TI</a></p>

         				</div>
         			</div>
         		</div>
         	</div>
         </section>
         <!-- END FOOTER -->


    <!-- =========================
         SCRIPTS 
         ============================== -->


         <script src="js/jquery.min.js"></script>
         <script src="js/bootstrap.min.js"></script>
         <script src="js/owl.carousel.js"></script>
         <script src="js/jquery.fitvids.js"></script>
         <script src="js/smoothscroll.js"></script>
         <script src="js/jquery.parallax-1.1.3.js"></script>
         <script src="js/jquery.prettyPhoto.js"></script>
         <script src="js/jquery.ajaxchimp.min.js"></script>
         <script src="js/jquery.ajaxchimp.langs.js"></script>
         <script src="js/wow.min.js"></script>
         <script src="js/waypoints.min.js"></script>
         <script src="js/jquery.counterup.min.js"></script>
         <script src="js/jquery.mask.min.js"></script>
         <script src="js/script.js"></script>

     </body>

     </html>
