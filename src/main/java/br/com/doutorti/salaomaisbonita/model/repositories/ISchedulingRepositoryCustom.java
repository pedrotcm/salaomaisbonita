package br.com.doutorti.salaomaisbonita.model.repositories;

import java.util.List;

import br.com.doutorti.salaomaisbonita.model.SchedulingEntity;

public interface ISchedulingRepositoryCustom {

	public List<SchedulingEntity> findCustom( SchedulingEntity schedulingFindEntity, boolean isHistory );

	public boolean existEqualsDate( SchedulingEntity scheduling );

}
