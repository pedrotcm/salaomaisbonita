package br.com.doutorti.salaomaisbonita.model.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.doutorti.salaomaisbonita.model.PersonEntity;

public interface IPersonRepository extends JpaRepository<PersonEntity, Long> {

}
